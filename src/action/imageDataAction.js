import UnsplashService from '../services/unsplash-service';

export function fetchDataImage() {
    return function (dispatch) {
        UnsplashService.getImages().then(res => {
            dispatch({
                type: 'dataImageList',
                payload: res.data.results,
            })
        })
    }
}

export function fetchQueryImage() {
    return function (dispatch) {
        UnsplashService.getSearchByPhoto().then(res => {
            dispatch({
                type: 'dataQueryImageList',
                payload: res.data,
            })
        })
    }
}
