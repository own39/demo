import { createStore, applyMiddleware } from 'redux';
import imageDataReducer from './reducer/imageDataReducer';
import thunk from 'redux-thunk';

const initialState = {};
const middleware = [thunk];
const store = createStore(
    imageDataReducer,
    initialState,
    applyMiddleware(...middleware)
);
export default store;