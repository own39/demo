import React from 'react';
import './component.scss';
import { connect } from 'react-redux';
import Footer from './footer';
import Header from './header';
import PropTypes from 'prop-types';
import { fetchQueryImage } from '../action/imageDataAction';

class Home extends React.Component {

  state = {
    data: [
      'Sham Pandey',
      'John Wick',
      'Jon Cena',
      'Jon Fan',
      'Roy Simo',
      'Raja Singh',
      'Manish Daga',
      'Tarun Sen',
      'Elena Gilbert',
      'Stephen'
    ]
  }

  constructor(props) {
    super(props)
    this.props.fetchQueryImage()
  }

  loadMore = () => {
    this.props.fetchQueryImage()
  }

  render() {

    return (
      <div>
        <Header />
        <div className="tabs-container-scroll">
          <ul>
            {
              this.props.dataQueryImageList &&
              this.props.dataQueryImageList.map((data, index) => {
                console.log("1 ", data)

                return (
                  <div>
                    <div>
                      <img alt="" src={data.urls.thumb} />
                    </div>
                    <li key={data.id}>
                      <div>
                        <div><h5>Name:- {data?.alt_description ?? this.state.data[index]}</h5></div>
                        {/* <div><h5>Desc:- {data.alt_description}</h5></div> */}
                        {/* <div style={{ textAlign: 'right' }}> <button className="btn-primary" onClick={this.viewPost}>View Post</button></div> */}
                      </div>
                    </li>
                  </div>)
              })
            }
          </ul>
          <button className="load-more"
            onClick={e => {
              this.loadMore();
            }}
          >
            Load More
          </button>
          <div>
            <button className="load-more"
              onClick={e => {
                sessionStorage.setItem('tokenValue', false);
                window.location = "/";
              }}
            >Logout</button>
          </div>
        </div>


        <Footer />
      </div>

    )
  }
}

Home.propTypes = {
  fetchQueryImage: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  dataQueryImageList: state.dataQueryImageList,
});
export default connect(mapStateToProps, { fetchQueryImage })(Home)
