import React from 'react';
import './component.scss';
import { connect } from 'react-redux';
import Footer from './footer';
import Header from './header'
import PropTypes from 'prop-types';
import { fetchQueryImage } from '../action/imageDataAction';

class ViewPost extends React.Component {

  constructor(props) {
    super(props)
    this.props.fetchQueryImage();
  }

  loadMore = () => {
    this.props.fetchQueryImage();
  }

  // programPattern = () => {
  //   let n = 7;
  //   for (let i = 1; i <= n; i++) {
  //     for (let j = 1; j <= i; j++) {
  //       console.log(" " + j)

  //     }
  //   }

  // }

  render() {

    return (
      <div>
        <Header />
        <div className="tabs-container-scroll">
          <ul>
            {
              this.props.dataQueryImageList &&
              this.props.dataQueryImageList.map((data) => {
                console.log("1 ", data)
                return (
                  <div>
                    <div>
                      <img alt="" src={data.urls.thumb} />
                    </div>
                    <li key={data.id}>
                      <div>
                        <div><h5>{data.alt_description}</h5></div>
                        <div></div>
                      </div>
                    </li>
                  </div>)
              })
            }
          </ul>
          <button className="load-more"
            onClick={e => {
              this.loadMore();
            }}
          >
            Load More
          </button>
        </div>
        <button className="load-more"
          onClick={e => {
            this.programPattern();
          }}
        >Pattern</button>
        <Footer />
      </div>

    )
  }
}

ViewPost.propTypes = {
  fetchQueryImage: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  dataQueryImageList: state.dataQueryImageList,
});

export default connect(mapStateToProps, { fetchQueryImage })(ViewPost)
