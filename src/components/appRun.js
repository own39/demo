import React from 'react';
import './component.scss';
import Login from './login';
import { fetchDataImage } from '../action/imageDataAction';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Footer from './footer';
import Header from './header';

class AppRun extends React.Component {

  constructor(props) {
    super(props)
    // this.props.fetchDataImage()
  }

  render() {
    return (
      <div className="container">
        <Header />
        <Login></Login>
        <Footer />
      </div>
    )
  }
}

export default (AppRun);
