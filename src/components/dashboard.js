import React from 'react';
import './component.scss';
import { connect } from 'react-redux';

class Dashboard extends React.Component {

  viewPost = () => {
    window.location = "/view-post";
  }

  render() {
    return (

      <div className="tabs-container-scroll">
        <ul>
          {
            this.props.dataImageList &&
            this.props.dataImageList.map((data) => {
              return (
                <div>
                  <div>
                    <img alt="" src={data.urls.thumb} />
                  </div>
                  <li key={data.id}>
                    <div>
                      <div><h5>{data.alt_description}</h5></div>
                      <div style={{ textAlign: 'right' }}> <button className="btn-primary" onClick={this.viewPost}>View Post</button></div>
                    </div>
                  </li>
                </div>)
            })
          }
        </ul>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  dataImageList: state.dataImageList,
});

export default connect(mapStateToProps)(Dashboard)
