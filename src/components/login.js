import React from 'react';
import './component.scss';
import { connect } from 'react-redux';

class Login extends React.Component {

  state = {
    username: '',
    password: '',
    error: 'Invalid Credential. Please Try Again',
    errorValue: false
  }

  componentWillMount() {
    sessionStorage.setItem('tokenValue', false)
  }

  loginClicked = () => {

    if (this.state.username === 'foo' && this.state.password === 'bar') {
      sessionStorage.setItem('username', this.state.username)
      sessionStorage.setItem('tokenValue', true)
      window.location = "/home";
    } else {
      this.setState({ errorValue: true })
    }

  }

  render() {
    return (

      <div className="tabs-container-scroll">
        User Name: <input type="text" name="username" onChange={(e) => { this.setState({ username: e.target.value }) }} />
        Password: <input type="password" name="password" onChange={(e) => { this.setState({ password: e.target.value }) }} />
        <button className="btn btn-success" onClick={this.loginClicked}>Login</button>
        {this.state.errorValue ? this.state.error : ''}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  dataImageList: state.dataImageList,
});

export default connect(mapStateToProps)(Login)
