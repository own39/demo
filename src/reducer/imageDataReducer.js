/* eslint-disable import/no-anonymous-default-export */
const initialState = {
    dataImageList: null,
    dataQueryImageList: []
}

export default function (state = initialState, action) {
    console.log("Redux Value", action.payload)
    switch (action.type) {
        case 'dataImageList':
            return {
                ...state,
                dataImageList: action.payload,
            }
        case 'dataQueryImageList':
            return {
                ...state,
                dataQueryImageList: action.payload,
            }
        default: return state;
    }
}

