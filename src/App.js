import './App.css';
import { BrowserRouter as Router } from "react-router-dom"
import Home from './components/home';
import AppRun from './components/appRun';
import { Route } from "react-router-dom";

function App() {

  return (
    <Router>
      <Route path='/' exact component={AppRun} />
      {
        sessionStorage.getItem('tokenValue') ?
          <Route path='/home/' component={Home} />
          : ''
      }

    </Router>
  );
}

export default App;
