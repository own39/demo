import axios from 'axios'

const UNSPLASH_REST_API_URL = 'https://api.unsplash.com/';
const ClIENT_SECRET_KEY = '5AmM8C_cf56ZqRdJBVmK3fRFDM0iZM59sFRY-UxcIbc';

class UnsplashService {

    getImages() {
        return axios.get(UNSPLASH_REST_API_URL + 'search/photos?page=1&query=laptop&client_id=' + ClIENT_SECRET_KEY);
    }

    getSearchByPhoto() {
        return axios.get(UNSPLASH_REST_API_URL + 'photos/random?count=8&client_id=' + ClIENT_SECRET_KEY);
    }

}
export default new UnsplashService();